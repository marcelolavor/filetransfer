#!$HOME/bin/bash
# Script para mover arquivos recebidos das operadoras para o Splitter e Joiner
ARQLOG=$1
FROM=$2
TO=$3
echo "from=$FROM"
echo "to=$TO"

if [ -d ${FROM} ]
then
	if [ -d ${TO} ]
	then
		# Salvo os arquivos enviados (por extensao!)
		echo "Moving files ${FROM}" | tee -a ${ARQLOG}
		FILELIST=`ls ${FROM} | sort | uniq | egrep -v .TST | egrep -v .TMP | egrep -v .tst | egrep -v .tmp`
		for FILE in ${FILELIST}
		do
			SIZE1=1
			SIZE2=2
			until [ "${SIZE1}" = "${SIZE2}" ]
			do
				SIZE1=`ls -la ${FROM}/${FILE} | awk '{print $5}'`
				/usr/bin/sleep $4
				SIZE2=`ls -la ${FROM}/${FILE} | awk '{print $5}'`
			done	
			if [ -f ${FROM}/${FILE} ] 
			then
				if cp -fp ${FROM}/${FILE} ${TO}/ | tee -a ${ARQLOG} 
				then
					if /usr/bin/dos2unix ${TO}/${FILE} ${TO}/${FILE}
					then 
					rm -rf ${FROM}/${FILE} | tee -a ${ARQLOG} 
					chmod 777 ${TO}/${FILE}
					echo "Moved ${FROM}/${FILE} to ${TO}/" | tee -a ${ARQLOG} 
					else 
					echo "Error converting dos2 unix on ${TO}/${FILE}" | tee -a ${ARQLOG} 
					fi
				else	
					echo "ERROR: Could�nt move file ${FROM}/${FILE} to ${TO}/" | tee -a ${ARQLOG} 
				fi
			else
				echo "ERROR: It not a file ${FROM}/${FILE}" | tee -a ${ARQLOG} 

			fi	
		done

	else
		echo "ERRO: Dir ${TO} doenst exists !!!" | tee -a ${ARQLOG}
	fi
else
	echo "ERRO: Diretorio do ${FROM} nao existe!!!" | tee -a ${ARQLOG}
fi

