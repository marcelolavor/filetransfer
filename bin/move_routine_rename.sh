#!/usr/local/bin/bash
# Script para mover arquivos recebidos das operadoras para o Splitter e Joiner

ARQLOG=$1
FROM=$2
TO=$3
ARQLOGIN=$4
echo "${ARQLOGIN}"
if [ -d ${FROM} ]
then
        echo "Renanimg files ${FROM}" | tee -a ${ARQLOG}
        #FILELIST=`ls ${FROM} | sort | uniq`
	FILELIST=`ls ${FROM} | sort | uniq | egrep -v .TST | egrep -v .TMP | egrep -v .tst | egrep -v .tmp`
        for FILE in ${FILELIST}
        do
		echo "-------------------------------------------------"
                echo "ACTUAL NAME:\t${FILE}"
                NEWNAME=$5`echo ${FILE} | cut -f$6 -d\$7`
                echo "NEW NAME:\t${NEWNAME}"
                echo "-------------------------------------------------"
                mkdir -p ${TO}
                if cp -fp ${FROM}/${FILE} ${TO}/${NEWNAME} | tee -a ${ARQLOG} 
                then
			/usr/bin/dos2unix ${TO}/${NEWNAME} ${TO}/${NEWNAME}
                        echo "Renamed ${FROM}/${FILE} to ${TO}/${NEWNAME}" | tee -a ${ARQLOG} 
                fi
        done
else
        echo "ERRO: Dir ${FROM} doesnt exitsts !!!" | tee -a ${ARQLOG}
fi
