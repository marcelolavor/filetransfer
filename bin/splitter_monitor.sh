#!/usr/local/bin/bash
#
# CHECK IF THERE ARE FILES INTO THE SPLITTER DIRECTORIES
#

# Get date
DATAFULL=`date '+%d_%m_%y_%h_%n'`

# Define Operator's mail
OPERATORS="monitor@timnet.com"

# Define Log and control file
ARQLOG="/tmp/splitter_report_${DATAFULL}.log"
DIRLISTFILE="/export/home/tbill/bin/dir_check.txt"

# Open Log file
> ${ARQLOG}

# Set file date criation
echo "Report Date: ${DATAFULL}" | tee -a ${ARQLOG}

# Read all directories to be checked
while read -r FROM
do
	echo "" | tee -a ${ARQLOG}
	echo "Directory: ${FROM}" | tee -a ${ARQLOG}
	echo "" | tee -a ${ARQLOG}
	FILELIST=`/bin/find ${FROM}/ -print`
	echo "${FILELIST}" | tee -a ${ARQLOG}
done < ${DIRLISTFILE} 

/bin/mail -s "Splitter monitor ${DATAFULL}" ${OPERATORS} < ${ARQLOG}
