#!/usr/local/bin/bash
# Script para mover arquivos recebidos das operadoras para o Splitter e Joiner
DIRBASECARRIER="/u01/timnet/carriers"
DIRBASESPLITTER="/u01/timnet/splitter"
CARRIERS=$1
IDFILE=$2
SLEEPTIME=0
ARQLOG="/tmp/movelog_${CARRIERS}_${IDFILE}.log"
MANAGERS="monitor@timnet.com"
#Inicializo os arquivos de log
> ${ARQLOG}

if [ "$1" = "" ]
then
	echo "                 MOVE SCRIPT"
	echo "----------------------------------------------"
	echo ""
	echo "1 - carrier identificator [mxt tcs tnc]"
	echo "2 - file identificator [opr|ret|sts|rsp|rqt]"
	echo ""
	echo "Ex: move.sh mxt opr"
	echo "To move Maxitel OPR files to splitter diretories"
	echo "Ex: move.sh \"mxt tnc tcs\" opr "
	echo "To transfer Maxitel,TNC and TCS OPR files to splitter directories"
	echo "----------------------------------------------"
	exit
fi	




DATAFULL=`date '+%d/%m/%Y %T %a'`
echo "In�cio: ${DATAFULL}" | tee -a ${ARQLOG}

for CARRIER in ${CARRIERS}
do
FROMPOSPAGO="${DIRBASECARRIER}/${CARRIER}/pos-pago"
TOPOSPAGO="${DIRBASESPLITTER}/prov/${CARRIER}/opr/input/pos-pago"
FROMPREPAGO="${DIRBASECARRIER}/${CARRIER}/pre-pago"
TOPREPAGO="${DIRBASESPLITTER}/prov/${CARRIER}/opr/input/pre-pago"
FROMFILERET="${DIRBASESPLITTER}/prov/${CARRIER}/ret/output"
TOFILERET="${DIRBASECARRIER}/${CARRIER}/FileRET"
FROMFILESTS="${DIRBASESPLITTER}/prov/${CARRIER}/sts/output"
TOFILESTS="${DIRBASECARRIER}/${CARRIER}/FileSTS"

FROMRSPPRE="${DIRBASECARRIER}/${CARRIER}/BLLRSPPRE"
TORSPPRE="${DIRBASESPLITTER}/bill/${CARRIER}/rsp/input/BLLRSPPRE"
FROMRSPPOS="${DIRBASECARRIER}/${CARRIER}/BLLRSPPOS"
TORSPPOS="${DIRBASESPLITTER}/bill/${CARRIER}/rsp/input/BLLRSPPOS"
FROMRQTPRE="${DIRBASESPLITTER}/bill/${CARRIER}/rqt/output/BLLRQTPRE"
TORQTPRE="${DIRBASECARRIER}/${CARRIER}/BLLRQTPRE"
FROMRQTPOS="${DIRBASESPLITTER}/bill/${CARRIER}/rqt/output/BLLRQTPOS"
TORQTPOS="${DIRBASECARRIER}/${CARRIER}/BLLRQTPOS"

ARQLOGINTIMNET=$HOME/etc/login_timnet_${CARRIER}.cfg
ARQLOGINTIMNETPROV=$HOME/etc/login_blah_${CARRIER}.cfg

if [ "${IDFILE}" = "opr" ] 
then	

        if [ "mxt" = ${CARRIER} ]
        then
                POS1=3
                POS2=2
                DIV1=S
                DIV2=C
        fi
        if [ "tcs" = ${CARRIER} ]
        then
                POS1=2
                POS2=3
                DIV1=L
                DIV2=t
        fi
        if [ "tnc" = ${CARRIER} ]
        then
                POS1=2
                POS2=3,4,5
                DIV1=c
                DIV2=p
        fi

	# SEND POS-PAID TO RING TONE APPLICATION
        /usr/bin/sh $HOME/bin/move_routine_rename.sh ${ARQLOG} ${FROMPOSPAGO} ${FROMPOSPAGO}/ro ${ARQLOGINTIMNET} TIMNETPOS ${POS1} ${DIV1}

	# SEND POS-PAID TO TIMNET BILL SERVER
        /usr/bin/sh $HOME/bin/move_routine_rename.sh ${ARQLOG} ${FROMPOSPAGO} ${FROMPOSPAGO}/timnet_prov ${ARQLOGINTIMNETPROV} TIMNETPOS ${POS1} ${DIV1}}

	# SEND POS-PAID TO MONITOR
        /usr/bin/sh $HOME/bin/move_routine_rename.sh ${ARQLOG} ${FROMPOSPAGO} /u01/timnet/splitter/monitor/${CARRIER} ${ARQLOGINTIMNET} TIMNETPOS ${POS1} ${DIV1}

	# SEND POS-PAID TO SPLITTER
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMPOSPAGO} ${TOPOSPAGO} ${SLEEPTIME}

        # SEND PRE-PAID TO RING TONE APPLICATION
        /usr/bin/sh $HOME/bin/move_routine_rename.sh ${ARQLOG} ${FROMPREPAGO} ${FROMPREPAGO}/ro ${ARQLOGINTIMNET} TIMNETPRP ${POS2} ${DIV2}

	# SEND PRE-PAID TO TIMNET BILL SERVER
        /usr/bin/sh $HOME/bin/move_routine_rename.sh ${ARQLOG} ${FROMPREPAGO} ${FROMPREPAGO}/timnet_prov ${ARQLOGINTIMNETPROV} TIMNETPRP ${POS2} ${DIV2}

        # SEND PRE-PAID TO MONITOR
        /usr/bin/sh $HOME/bin/move_routine_rename.sh ${ARQLOG} ${FROMPREPAGO} /u01/timnet/splitter/monitor/${CARRIER} ${ARQLOGINTIMNET} TIMNETPRP ${POS2} ${DIV2}

	# SEND PRE-PAID TO SPLITTER
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMPREPAGO} ${TOPREPAGO} ${SLEEPTIME}
fi

if [ "${IDFILE}" = "ret" ] 
then	
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMFILERET} ${TOFILERET} ${SLEEPTIME}
fi
if [ "${IDFILE}" = "sts" ] 
then	
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMFILESTS} ${TOFILESTS} ${SLEEPTIME}
fi
if [ "${IDFILE}" = "rsp" ] 
then	
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMRSPPRE} ${TORSPPRE} ${SLEEPTIME}
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMRSPPOS} ${TORSPPOS} ${SLEEPTIME}
fi
if [ "${IDFILE}" = "rqt" ] 
then	
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMRQTPRE} ${TORQTPRE} ${SLEEPTIME}
	/usr/bin/sh $HOME/bin/move_routine.sh ${ARQLOG} ${FROMRQTPOS} ${TORQTPOS} ${SLEEPTIME}
fi

done


DATAFULL=`date '+%d/%m/%y %T %a'`
echo "Fim: ${DATAFULL}" | tee -a ${ARQLOG}
#/bin/mail -s "Move Files" ${MANAGERS} < ${ARQLOG}
#/usr/bin/mailx -s "Moving ${CARRIERS} ${IDFILE} Files" ${MANAGERS} < ${ARQLOG}
