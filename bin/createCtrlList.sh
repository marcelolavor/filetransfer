#!/usr/bin/ksh

#set -o xtrace

BASEDIR=/u01/timnet/splitter/control_lists
USERSDELETEDDIR=${BASEDIR}/usersdeleted
BINDIR=/export/home/tbill/bin
TMPDIR=/tmp

# Format yyymmdd
lastdate=`cat ${BINDIR}/createCtrlList.cfg` 
filenamelastdate=`echo ${lastdate} | awk '{print substr($1,3)}'`
LASTPROCESSEDDIR=${BASEDIR}/Processed/${lastdate}

datefull=`date '+%Y%m%d'`
#datefull="20010928"
filenamedate=`date '+%y%m%d'`
#filenamedate="010928"

> ${BINDIR}/createCtrlList.log

PROCESSEDDIR=${BASEDIR}/Processed/${datefull}
mkdir ${PROCESSEDDIR}

ncftpget -a -V -u maxitelprov -p timnet2001 10.2.4.6 ${PROCESSEDDIR} registeredusers/list${filenamedate}.txt
mv ${PROCESSEDDIR}/list${filenamedate}.txt ${PROCESSEDDIR}/MXTlist${filenamedate}.txt

ncftpget -a -V -u tcsprov -p pwdtcsprov01 10.2.4.8 ${PROCESSEDDIR} registeredusers/list${filenamedate}.txt
mv ${PROCESSEDDIR}/list${filenamedate}.txt ${PROCESSEDDIR}/TCSlist${filenamedate}.txt

#ncftpget -a -V -u tncprov -p pwdtncprov02 10.2.4.10 ${PROCESSEDDIR} registeredusers/list${filenamedate}.txt
#mv ${PROCESSEDDIR}/list${filenamedate}.txt ${PROCESSEDDIR}/TNClist${filenamedate}.txt

sed 's/ //' ${LASTPROCESSEDDIR}/MXTctrllist${filenamelastdate}.txt > /tmp/createCtrlList.tmp
mv /tmp/createCtrlList.tmp ${LASTPROCESSEDDIR}/MXTctrllist${filenamelastdate}.txt
sort -o ${LASTPROCESSEDDIR}/MXTctrllist${filenamelastdate}.txt ${LASTPROCESSEDDIR}/MXTctrllist${filenamelastdate}.txt 
sed 's/ //' ${PROCESSEDDIR}/MXTlist${filenamedate}.txt  > /tmp/createCtrlList.tmp
mv /tmp/createCtrlList.tmp ${PROCESSEDDIR}/MXTlist${filenamedate}.txt
sort -o ${PROCESSEDDIR}/MXTlist${filenamedate}.txt ${PROCESSEDDIR}/MXTlist${filenamedate}.txt

sed 's/ //' ${LASTPROCESSEDDIR}/TCSctrllist${filenamelastdate}.txt > /tmp/createCtrlList.tmp
mv /tmp/createCtrlList.tmp ${LASTPROCESSEDDIR}/TCSctrllist${filenamelastdate}.txt
sort -o ${LASTPROCESSEDDIR}/TCSctrllist${filenamelastdate}.txt ${LASTPROCESSEDDIR}/TCSctrllist${filenamelastdate}.txt 
sed 's/ //' ${PROCESSEDDIR}/TCSlist${filenamedate}.txt > /tmp/createCtrlList.tmp
mv /tmp/createCtrlList.tmp ${PROCESSEDDIR}/TCSlist${filenamedate}.txt
sort -o ${PROCESSEDDIR}/TCSlist${filenamedate}.txt ${PROCESSEDDIR}/TCSlist${filenamedate}.txt

sed 's/ //g' ${LASTPROCESSEDDIR}/TNCctrllist${filenamelastdate}.txt > /tmp/createCtrlList.tmp
mv /tmp/createCtrlList.tmp ${LASTPROCESSEDDIR}/TNCctrllist${filenamelastdate}.txt
sort -o ${LASTPROCESSEDDIR}/TNCctrllist${filenamelastdate}.txt ${LASTPROCESSEDDIR}/TNCctrllist${filenamelastdate}.txt 
sed 's/ //g' ${PROCESSEDDIR}/TNClist${filenamedate}.txt > /tmp/createCtrlList.tmp
mv /tmp/createCtrlList.tmp ${PROCESSEDDIR}/TNClist${filenamedate}.txt
sort -o ${PROCESSEDDIR}/TNClist${filenamedate}.txt ${PROCESSEDDIR}/TNClist${filenamedate}.txt

comm -23 ${LASTPROCESSEDDIR}/MXTctrllist${filenamelastdate}.txt ${PROCESSEDDIR}/MXTlist${filenamedate}.txt > ${PROCESSEDDIR}/MXTdeletedlist${filenamedate}.txt
comm -12 ${LASTPROCESSEDDIR}/MXTctrllist${filenamelastdate}.txt ${PROCESSEDDIR}/MXTlist${filenamedate}.txt > ${PROCESSEDDIR}/MXTctrllist${filenamedate}.txt

comm -23 ${LASTPROCESSEDDIR}/TCSctrllist${filenamelastdate}.txt ${PROCESSEDDIR}/TCSlist${filenamedate}.txt > ${PROCESSEDDIR}/TCSdeletedlist${filenamedate}.txt
comm -12 ${LASTPROCESSEDDIR}/TCSctrllist${filenamelastdate}.txt ${PROCESSEDDIR}/TCSlist${filenamedate}.txt > ${PROCESSEDDIR}/TCSctrllist${filenamedate}.txt

comm -23 ${LASTPROCESSEDDIR}/TNCctrllist${filenamelastdate}.txt ${PROCESSEDDIR}/TNClist${filenamedate}.txt > ${PROCESSEDDIR}/TNCdeletedlist${filenamedate}.txt
comm -12 ${LASTPROCESSEDDIR}/TNCctrllist${filenamelastdate}.txt ${PROCESSEDDIR}/TNClist${filenamedate}.txt > ${PROCESSEDDIR}/TNCctrllist${filenamedate}.txt

cp ${PROCESSEDDIR}/MXTctrllist${filenamedate}.txt ${BASEDIR}/mxt_control_list.txt
cp ${PROCESSEDDIR}/TCSctrllist${filenamedate}.txt ${BASEDIR}/tcs_control_list.txt
cp ${PROCESSEDDIR}/TNCctrllist${filenamedate}.txt ${BASEDIR}/tnc_control_list.txt

cat ${PROCESSEDDIR}/MXTdeletedlist${filenamedate}.txt ${PROCESSEDDIR}/TCSdeletedlist${filenamedate}.txt ${PROCESSEDDIR}/TNCdeletedlist${filenamedate}.txt > ${PROCESSEDDIR}/Usersdeleted${filenamedate}.txt
  
cp ${PROCESSEDDIR}/Usersdeleted${filenamedate}.txt ${USERSDELETEDDIR}/acotel_delete.txt

comm -13 ${LASTPROCESSEDDIR}/MXTlist${filenamelastdate}.txt ${PROCESSEDDIR}/MXTlist${filenamedate}.txt > ${PROCESSEDDIR}/MXTaddedlist${filenamedate}.txt

comm -13 ${LASTPROCESSEDDIR}/TCSlist${filenamelastdate}.txt ${PROCESSEDDIR}/TCSlist${filenamedate}.txt > ${PROCESSEDDIR}/TCSaddedlist${filenamedate}.txt

comm -13 ${LASTPROCESSEDDIR}/TNClist${filenamelastdate}.txt ${PROCESSEDDIR}/TNClist${filenamedate}.txt > ${PROCESSEDDIR}/TNCaddedlist${filenamedate}.txt

echo MXT `cat ${PROCESSEDDIR}/MXTaddedlist${filenamedate}.txt | wc -l` > /tmp/createCtrllist_$$.tmp
echo TCS `cat ${PROCESSEDDIR}/TCSaddedlist${filenamedate}.txt | wc -l` >> /tmp/createCtrllist_$$.tmp
echo TNC `cat ${PROCESSEDDIR}/TNCaddedlist${filenamedate}.txt | wc -l` >> /tmp/createCtrllist_$$.tmp

mailx -s "Users Added on Acotel" hsantos@corp.timnet.com < /tmp/createCtrllist_$$.tmp

rm /tmp/createCtrllist_$$.tmp

echo ${datefull} > ${BINDIR}/createCtrlList.cfg
