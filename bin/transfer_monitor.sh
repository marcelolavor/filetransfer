#!/usr/local/bin/bash
# script  to send and receive files using ncftp
# author: Bruno Torres Goyanna
# version: $Id$

DIRBASECARRIER="/u01/timnet/splitter/monitor"
CARRIER=$1
IDFILE=$2
ARQLOG="/tmp/transferlog_${CARRIER}_MONITOR'.log"
MANAGERS="monitor@timnet.com"
#Inicializo os arquivos de log
> ${ARQLOG}

if [ "$1" = "" ]
then
        echo "            FTP Transfer MONITOR"
        echo "----------------------------------------------"
        echo ""
        echo "1 - carrier identificator [mxt tcs tnc]"
        echo ""
        echo "Ex: transfer_monitor.sh mxt "
        echo "To transfer Maxitel"
        echo "----------------------------------------------"
        exit
fi      



DATAFULL=`date '+%d/%m/%Y %T %a'`
echo "Transfering ${IDFILE} to monitor at ${DATAFULL}" | tee -a ${ARQLOG}
FROM="${DIRBASECARRIER}/${CARRIER}"
TO="./rt/${CARRIER}"
ARQLOGIN=$HOME/etc/login_timnet_monitor.cfg

/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${FROM} ${TO} . ${ARQLOGIN}

DATAFULL=`date '+%d/%m/%y %T %a'`
echo "Finished at ${DATAFULL}" | tee -a ${ARQLOG}
#/bin/mail -s "Transfer Files" ${MANAGERS} < ${ARQLOG}
/usr/bin/mailx -s "${CARRIER} - Ring tones files transfer" ${MANAGERS} < ${ARQLOG}
