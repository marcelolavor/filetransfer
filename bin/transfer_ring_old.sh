#!/usr/local/bin/bash
# script  to send and receive files using ncftp
# author: Bruno Torres Goyanna
# version: $Id$

DIRBASECARRIER="/u01/timnet/carriers"
CARRIER=$1
IDFILE=$2
ARQLOG="/tmp/transferlog_${CARRIERS}_RING.log"
MANAGERS="monitor@timnet.com"
#Inicializo os arquivos de log
> ${ARQLOG}

if [ "$1" = "" ]
then
        echo "            FTP Transfer RING TONE"
        echo "----------------------------------------------"
        echo ""
        echo "1 - carrier identificator [mxt tcs tnc]"
        echo ""
        echo "Ex: transfer_ring.sh mxt "
        echo "To transfer Maxitel"
        echo "----------------------------------------------"
        exit
fi      



DATAFULL=`date '+%d/%m/%Y %T %a'`
echo "Transfering ${IDFILE} to Ring Tones at ${DATAFULL}" | tee -a ${ARQLOG}
FROMPOSPAGO="${DIRBASECARRIER}/${CARRIER}/pos-pago/ro"
TOPOSPAGO="."
FROMPREPAGO="${DIRBASECARRIER}/${CARRIER}/pre-pago/ro"
TOPREPAGO="."
ARQLOGIN=$HOME/etc/login_timnet_${CARRIER}.cfg

/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${FROMPOSPAGO} ${TOPOSPAGO} . ${ARQLOGIN}
/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${FROMPREPAGO} ${TOPREPAGO} . ${ARQLOGIN}

DATAFULL=`date '+%d/%m/%y %T %a'`
echo "Finished at ${DATAFULL}" | tee -a ${ARQLOG}
#/bin/mail -s "Transfer Files" ${MANAGERS} < ${ARQLOG}
/usr/bin/mailx -s "${CARRIER} - Ring tones files transfer" ${MANAGERS} < ${ARQLOG}
