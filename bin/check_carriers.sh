#!/usr/local/bin/bash
#
# CHECK IF THERE ARE FILES INSIDE THE CARRIERS DIRECTORIES
#

# Get date
DATAFULL=`date '+%d_%m_%y_%h_%n'`

CARRIER=$1
LOCAL=$2
EMAIL=$3

# Define Operator's mail
#OPERATORS="monitor@timnet.com"

# Define Log and control file
ARQLOG="/tmp/check_report.tmp"

DIRLISTFILE="/export/home/tbill/bin/dir_check.txt"


if LOCAL="carrier"
then
	DIRBASE="/u01/timnet/carriers/${CARRIER}"
fi

# Check if there is some file there
RQTPRE=`ls -la ${DIRBASE}/BLLRQTPRE | wc -l`
RQTPOS=`ls -la ${DIRBASE}/BLLRQTPOS | wc -l`
RSPPRE=`ls -la ${DIRBASE}/BLLRSPPRE | wc -l`
RSPPOS=`ls -la ${DIRBASE}/BLLRSPPOS | wc -l`
PREPAG=`ls -la ${DIRBASE}/pre-pago | wc -l`
POSPAG=`ls -la ${DIRBASE}/pos-pago | wc -l`

RQTPRE=`expr ${RQTPRE} - 3`
RQTPOS=`expr ${RQTPOS} - 3`
RSPPRE=`expr ${RSPPRE} - 3`
RSPPOS=`expr ${RSPPOS} - 3`
PREPAG=`expr ${PREPAG} - 4`
POSPAG=`expr ${POSPAG} - 4`

echo "${RQTPRE} ${RQTPOS} ${RSPPRE} ${RSPPOS} ${PREPAG} ${POSPAG}"
exit

# Open Log file
> ${ARQLOG}

# Set file date criation
echo "Report Date: ${DATAFULL}" | tee -a ${ARQLOG}

# Read all directories to be checked
while read -r FROM
do
        echo "" | tee -a ${ARQLOG}
        echo "Directory: ${FROM}" | tee -a ${ARQLOG}
        echo "" | tee -a ${ARQLOG}
        FILELIST=`/bin/find ${FROM}/ -print`
        echo "${FILELIST}" | tee -a ${ARQLOG}
done < ${DIRLISTFILE} 

/bin/mail -s "Splitter monitor ${DATAFULL}" ${OPERATORS} < ${ARQLOG}
