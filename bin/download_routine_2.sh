#!/usr/local/bin/bash
# Script para mover arquivos recebidos das operadoras para o Splitter e Joiner
ARQLOG=$1
FROM=$2
TO=$3
PORTAL=$4
URL=ftp://$5
ARQLOGIN=$6
EXTENSAO=$7
echo "url=$URL"
echo "from=$FROM"
echo "to=$TO"
echo '-----------------------'
echo "1=$1"
echo "2=$2"
echo "3=$3"
echo "4=$4"
echo "5=$5"
echo "6=$6"
echo "7=$7"
echo '-----------------------' 
COUNT=`/usr/local/bin/ncftpls -1 -f ${ARQLOGIN} ${URL}/${FROM}/*.${EXTENSAO} | wc -l`

if [ ${COUNT} -gt 0 ]
then
	if [ -d ${TO} ]
	then
        	echo "Source: ${FROM}" | tee -a ${ARQLOG}
		FILELIST=`/usr/local/bin/ncftpls -1 -f ${ARQLOGIN} ${URL}/${FROM}/*.${EXTENSAO} | sort | uniq`
		if [ "${FILELIST}" = "" ]
        	then
               		 echo "No files" | tee -a ${ARQLOG}
        	fi	
		for FILE in ${FILELIST}
		do
			#if [ -f ${FILE} ]
                	#then
				echo "Moving file: ${FILE}" | tee -a ${ARQLOG}
				mkdir -p ${TO}/tmp
				if /usr/local/bin/ncftpget -a -DD -f ${ARQLOGIN} ${TO}/tmp ${FROM}/${FILE} 
				then
					mv ${TO}/tmp/${FILE} ${TO}/
					echo "Downloaded file ${URL}/${FROM}/${FILE}" | tee -a ${ARQLOG} 
				else
					echo "ERROR: Error while downloading file ${URL}/${FROM}/${FILE}" | tee -a ${ARQLOG} 

				fi	
                	#else
                        #	echo ${FILE}
                	#fi
		done

	else
		echo "ERROR: Dir ${TO} does not exist !!!" | tee -a ${ARQLOG}
	fi
else
	echo "No file to be downloaded" | tee -a ${ARQLOG}
fi
