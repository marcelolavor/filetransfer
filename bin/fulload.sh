#!/usr/local/bin/bash
#
#
# Script para realizar full load de arquivo da maxitel pre-pago e pos-pago
# Author: Marcelo Lavor
#

#
#Inicializo os arquivos de log
#

if [ "$1" = "" ]
then
        echo "                 fulload.sh SCRIPT"
        echo "----------------------------------------------"
        echo ""
        echo "1 - carrier name (mxt, tnc, tcs)"
        echo "2 - type of full provisioning filename (pre/pos)"
        echo "3 - carrier full provisioning filename"
        echo ""
        echo "Ex: fulload.sh mxt pre OPSC20011021171000.OPR"
        echo "To process Maxitel pre-paid specific full provisioning file"
        echo "----------------------------------------------"
        exit
fi      

CARRIER=$1
TYPE=$2
FILE=$3

DIRBASECARRIER="/u01/timnet/carriers"
DIRBASESPLITTER="/u01/timnet/splitter"
FROM="${DIRBASECARRIER}/${CARRIER}/${TYPE}-pago"
TO_RT="${FROM}/ro"
TO_MONITOR="/u01/timnet/splitter/monitor/${CARRIER}"
TO_SPLITTER="${DIRBASESPLITTER}/prov/${CARRIER}/opr/input/${TYPE}-pago"

if TYPE="pre"
then
	NEWNAME=TIMNETPRP`echo ${FILE} | cut -f2 -d\C`
else
        NEWNAME=TIMNETPRP`echo ${FILE} | cut -f3 -d\S`
fi

ARQLOG="/tmp/${CARRIER}_${TYPE}pago_full_${FILE}.log"
MANAGERS="mlavor@corp.timnet.com"
SLEEPTIME=0
DATAFULL=`date '+%d/%m/%Y %T %a'`

> ${ARQLOG}

echo "Inicio: ${DATAFULL}" | tee -a ${ARQLOG}


#
# CHECK IF FILE TRANSFERING HAS FINISHED
#

echo "Checking file name ..."
echo

SIZE1=1
SIZE2=2
until [ "${SIZE1}" = "${SIZE2}" ]
do
        SIZE1=`ls -la ${FROM}/${FILE} | awk '{print $5}'`
        /usr/bin/sleep ${SLEEPTIME}
        SIZE2=`ls -la ${FROM}/${FILE} | awk '{print $5}'`
done    
echo "ok"

#
# MOVING TO RING TONES
#
echo "**********************************************************************"
echo "*"
echo "Transfering to ring tones file ${FILE}... as ... ${NEWNAME}"
echo "*"
echo "**********************************************************************"

mkdir -p ${TO_RT}
if cp -fp ${FROM}/${FILE} ${TO_RT}/${NEWNAME} | tee -a ${ARQLOG} 
then
	/usr/bin/dos2unix ${TO_RT}/${NEWNAME} ${TO_RT}/${NEWNAME}
        echo "File ${FROM}/${FILE} sent to ${TO_RT}/${NEWNAME}" | tee -a ${ARQLOG} 
fi


#
# MOVING TO MONITOR
#

echo "**********************************************************************"
echo "*"
echo "Transfering to monitor file ${FILE}... as ... ${NEWNAME}"
echo "*"
echo "**********************************************************************"

mkdir -p ${TO_MONITOR}
if cp -fp ${FROM}/${FILE} ${TO_MONITOR}/${NEWNAME} | tee -a ${ARQLOG} 
then
	/usr/bin/dos2unix ${TO_MONITOR}/${NEWNAME} ${TO_MONITOR}/${NEWNAME}
        echo "File ${FROM}/${FILE} sent to ${TO_MONITOR}/${NEWNAME}" | tee -a ${ARQLOG} 
fi


#
# MOVING TO SPLITTER
#

echo "**********************************************************************"
echo "*"
echo "Transfering ${FILE} to splitter"
echo "*"
echo "**********************************************************************"

if cp -fp ${FROM}/${FILE} ${TO_SPLITTER}/ | tee -a ${ARQLOG} 
then
    if /usr/bin/dos2unix ${TO_SPLITTER}/${FILE} ${TO_SPLITTER}/${FILE}
    then 
        rm -rf ${FROM}/${FILE} | tee -a ${ARQLOG} 
        chmod 777 ${TO_SPLITTER}/${FILE}
        echo "Moved ${FROM}/${FILE} to ${TO_SPLITTER}/" | tee -a ${ARQLOG} 
    else 
        echo "*** ALERT *** Error converting dos2 unix on ${TO}/${FILE}" | tee -a ${ARQLOG} 
    fi
else    
    echo "*** ALERT *** Error: Could not move file ${FROM}/${FILE} to ${TO_SPLITTER}/" | tee -a ${ARQLOG} 
fi



#
# SPLITTING THE FILE
#

echo "**********************************************************************"
echo "*"
echo "Splitting file ${FILE}"
echo "*"
echo "**********************************************************************"

splitter.pl ${CARRIER} prov 1 0 0


#
# UPLOADING TO ACOTEL AND WYSDOM
#

DIRBASEPORTAL="."
UPFROMACOTEL="${DIRBASESPLITTER}/prov/${CARRIER}/opr/output/acotel/${TYPE}-pago"
UPFROMWYSDOM="${DIRBASESPLITTER}/prov/${CARRIER}/opr/output/wysdom/${TYPE}-pago"
UPTOPREPAGO="${DIRBASEPORTAL}/${TYPE}-pago"
ARQLOGIN_ACOTEL=$HOME/etc/login_acotel_${CARRIER}.cfg
ARQLOGIN_WYSDOM=$HOME/etc/login_wysdom_${CARRIER}.cfg

echo "**********************************************************************"
echo "*"
echo "Uploading file ${FILE} to Acotel"
echo "*"
echo "**********************************************************************"

/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${UPFROMACOTEL} ${UPTOPREPAGO} acotel ${ARQLOGIN_ACOTEL}

echo "**********************************************************************"
echo "*"
echo "Uploading file ${FILE} to Wysdom"
echo "*"
echo "**********************************************************************"

/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${UPFROMWYSDOM} ${UPTOPREPAGO} wysdom ${ARQLOGIN_WYSDOM}

DATAFULL=`date '+%d/%m/%y %T %a'`
echo "Fim: ${DATAFULL}" | tee -a ${ARQLOG}

/usr/bin/mailx -s "${CARRIER} full provisioning file load: ${FILE}" ${MANAGERS} < ${ARQLOG}
