#!/usr/local/bin/bash
# Script para mover arquivos recebidos das operadoras para o Splitter e Joiner

ARQLOG=$1
FROM=$2
TO=$3
PORTAL=$4
ARQLOGIN=$5
echo "from=$FROM"
echo "to=$TO"
MANAGERS="mlavor@timnet.com"

#ARQLOGIN=$HOME/etc/login_${PORTAL}_${CARRIER}.cfg

if [ -d ${FROM} ]
then
	echo "Source: ${FROM}" | tee -a ${ARQLOG}
	#FILELIST=`ls ${FROM} | sort | uniq`
	FILELIST=`ls ${FROM} | sort | uniq | egrep -v .TST | egrep -v .TMP | egrep -v .tst | egrep -v .tmp`

	if [ "${FILELIST}" = "" ]
	then
		echo "No files" | tee -a ${ARQLOG}
	fi
	for FILE in ${FILELIST}
	do
		if [ -f ${FROM}/${FILE} ]
		then
			echo "Moving file: ${FILE}" | tee -a ${ARQLOG}
			cp ${FROM}/${FILE} /tmp/${PORTAL}/
               		FILESIZE=`ls -l ${FROM}/${FILE} | awk '{print $5}'`
                	if [ ${FILESIZE} -lt 10 ]
               		then
                       		/usr/bin/mailx -s "*** ALERT ${CARRIER} *** File ${FROM}/${FILE} with size less then 10 bytes" ${MANAGERS} < ${FROM}/${FILE}
			fi
			if /usr/local/bin/ncftpput -a -DD -z -S ".tmp" -f ${ARQLOGIN} ${TO} ${FROM}/${FILE} | tee -a ${ARQLOG}
			then
				echo "Sent ${FROM}/${FILE} to ${TO}/  (${FILESIZE})" | tee -a ${ARQLOG} 
			else
				echo "ERRO: Didnt send ${FROM}/${FILE}" | tee -a ${ARQLOG} 
			fi	
		else
			echo ${FILE}
		fi
	done
else
	echo "ERRO: Dir ${FROM} doesnt exitsts !!!" | tee -a ${ARQLOG}
fi
