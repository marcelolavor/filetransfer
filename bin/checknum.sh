#!/usr/bin/ksh
# Script to check if the number is provisioned or not
# Author: Marcelo Lavor
#

#set -o xtrace

DIRBASESPLITTER="/u01/timnet/splitter"
MOBILENUM=$1
TEMPFILE="/tmp/checknum_$$.tmp" 

if [ "$1" = "" ]
then
 echo "                 checknum SCRIPT"
 echo "----------------------------------------------"
 echo ""
 echo "1 - mobile number complete [e.g 03191018723]"
 echo ""
 echo "Ex: checknum.sh 03191018723"
 echo "----------------------------------------------"
 exit
fi 

echo ""
echo "---------------------------------------"

PREFIXOTEL=`echo $MOBILENUM | awk '{print substr($0,1,3)}'`

case $PREFIXOTEL in 
     031 | 032 | 033 | 034 | 035 | 037 | 038 | 071 | 073 | 074 | 075 | 077 | 079)
         CARRIER="mxt";;
     041 | 042 | 043 | 044 | 045 | 046 | 047 | 048 | 049 | 053)
         CARRIER="tcs";;
     081 | 082 | 083 | 084 | 085 | 086 | 088)
         CARRIER="tnc";;
     *)
         echo Numero de telefone invalido
         exit;;
esac

DIR="${DIRBASESPLITTER}/prov/${CARRIER}/opr/backup/pre-pago/"

#echo ${MOBILENUM} 
echo ${DIR} 
#echo ${TEMPFILE} 
grep ${MOBILENUM} ${DIR} > ${TEMPFILE} 

PRESTATUS=`wc -l ${TEMPFILE}`
PRESTATUS=`echo ${PRESTATUS% *}`

if [ ${PRESTATUS} -gt 0 ]
then
	echo "${CARRIER} PRE-PAID number"
        cat ${TEMPFILE} 
        #awk '{ print $0}' /tmp/checknum.tmp
fi


DIR="${DIRBASESPLITTER}/prov/${CARRIER}/opr/backup/pos-pago/*"

grep ${MOBILENUM} ${DIR} > ${TEMPFILE} 

POSSTATUS=`wc -l ${TEMPFILE}`
POSSTATUS=`echo ${POSSTATUS% *}`

if [ ${POSSTATUS} -gt 0 ]
then
	echo "${CARRIER} POS-PAID number"
        cat ${TEMPFILE}
fi

rm ${TEMPFILE}

FILE="${DIRBASESPLITTER}/control_lists/${CARRIER}_control_list.txt"
CONTROLLISTSTATUS=`grep ${MOBILENUM} ${FILE}`

if [ "${CONTROLLISTSTATUS}" = "" ]
then
	echo "This number do NOT belong to Acotel"
else
	echo "This number belong to Acotel"
fi

echo "---------------------------------------"
echo ""
