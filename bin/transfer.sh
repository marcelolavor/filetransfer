#!/usr/local/bin/bash
# script  to send and receive files using ncftp
# author: Bruno Torres Goyanna
# version: $Id$

DIRBASEPORTAL="."
DIRBASESPLITTER="/u01/timnet/splitter"
CARRIERS=$1
IDFILE=$2
PORTALS=$3
IPACOTELMXT=10.2.4.6
IPACOTELTNC=10.2.4.10
IPACOTELTCS=10.2.4.8
IPWYSDOMMXT=192.168.1.122
IPWYSDOMTNC=192.168.1.122
IPWYSDOMTCS=192.168.1.122
DATAFULL=`date '+%d/%m/%Y %T %a'`
TIMESTAMP=`date '+%H%M%S'`
ARQLOG="/tmp/transferlog_${CARRIERS}_${IDFILE}_${PORTALS}_${TIMESTAMP}.log"
MANAGERS="monitor@timnet.com"
#Inicializo os arquivos de log
> ${ARQLOG}

if [ "$1" = "" ]
then
	echo "            FTP Transfer SCRIPT"
	echo "----------------------------------------------"
	echo ""
	echo "1 - carrier identificator [mxt tcs tnc]"
	echo "2 - file identificator [opr|ret|sts|rsp|rqt]"
	echo "3 - plataform identificator [wysdom acotel]"
	echo ""
	echo "Ex: transfer.sh mxt opr acotel"
	echo "To transfer Maxitel OPR file to acotel"
	echo "Ex: transfer.sh \"mxt tnc tcs\" opr \"acotel wysdom\" "
	echo "To transfer Maxitel,TNC and TCS OPR files to acotel and wysdom"
	echo "----------------------------------------------"
	exit
fi	

DATAFULL=`date '+%d/%m/%Y %T %a'`
echo "Transfering ${IDFILE} files(${CARRIERS}) at ${DATAFULL}" | tee -a ${ARQLOG}

for CARRIER in ${CARRIERS}
do

	for PORTAL in ${PORTALS}
	do
		if [ "${PORTAL}" = "acotel" ]
		then
			if [ "${CARRIER}" = "tnc" ]
			then
				IP=${IPACOTELTNC}
			fi
			if [ "${CARRIER}" = "tcs" ]
			then
				IP=${IPACOTELTCS}
			fi
			if [ "${CARRIER}" = "mxt" ]
			then
				IP=${IPACOTELMXT}
			fi
			DOWNFROMFILERET="${DIRBASEPORTAL}/FileRET"
			DOWNTOFILERET="${DIRBASESPLITTER}/prov/${CARRIER}/ret/input/${PORTAL}"
			DOWNFROMFILESTS="${DIRBASEPORTAL}/FileSTS"
			DOWNTOFILESTS="${DIRBASESPLITTER}/prov/${CARRIER}/sts/input/${PORTAL}"

			UPFROMRSPPRE="${DIRBASESPLITTER}/bill/${CARRIER}/rsp/output/${PORTAL}/BLLRSPPRE"
			UPTORSPPRE="${DIRBASEPORTAL}/BLLRSPPRE"
			UPFROMRSPPOS="${DIRBASESPLITTER}/bill/${CARRIER}/rsp/output/${PORTAL}/BLLRSPPOS"
			UPTORSPPOS="${DIRBASEPORTAL}/BLLRSPPOS"

			DOWNFROMRQTPRE="${DIRBASEPORTAL}/BLLRQTPRE"
			DOWNTORQTPRE="${DIRBASESPLITTER}/bill/${CARRIER}/rqt/output/BLLRQTPRE"
			DOWNFROMRQTPOS="${DIRBASEPORTAL}/BLLRQTPOS"
			DOWNTORQTPOS="${DIRBASESPLITTER}/bill/${CARRIER}/rqt/output/BLLRQTPOS"


		fi
		if [ "${PORTAL}" = "wysdom" ]
		then
			if [ "${CARRIER}" = "tnc" ]
			then
				IP=${IPWYSDOMTNC}
			fi
			if [ "${CARRIER}" = "tcs" ]
			then
				IP=${IPWYSDOMTCS}
			fi
			if [ "${CARRIER}" = "mxt" ]
			then
				IP=${IPWYSDOMMXT}
			fi

			DOWNFROMFILERET="${DIRBASEPORTAL}/fileret"
			DOWNTOFILERET="${DIRBASESPLITTER}/prov/${CARRIER}/ret/input/${PORTAL}"
			DOWNFROMFILESTS="${DIRBASEPORTAL}/filests"
			DOWNTOFILESTS="${DIRBASESPLITTER}/prov/${CARRIER}/sts/input/${PORTAL}"

			UPFROMRSPPRE="${DIRBASESPLITTER}/bill/${CARRIER}/rsp/output/${PORTAL}/BLLRSPPRE"
			UPTORSPPRE="${DIRBASEPORTAL}/bllrsppre"
			UPFROMRSPPOS="${DIRBASESPLITTER}/bill/${CARRIER}/rsp/output/${PORTAL}/BLLRSPPOS"
			UPTORSPPOS="${DIRBASEPORTAL}/bllrsppos"

			DOWNFROMRQTPRE="${DIRBASEPORTAL}/bllrqtpre"
			DOWNTORQTPRE="${DIRBASESPLITTER}/bill/${CARRIER}/rqt/output/BLLRQTPRE"
			DOWNFROMRQTPOS="${DIRBASEPORTAL}/bllrqtpos"
			DOWNTORQTPOS="${DIRBASESPLITTER}/bill/${CARRIER}/rqt/output/BLLRQTPOS"
		fi

		UPFROMPOSPAGO="${DIRBASESPLITTER}/prov/${CARRIER}/opr/output/${PORTAL}/pos-pago"
		UPTOPOSPAGO="${DIRBASEPORTAL}/pos-pago"
		UPFROMPREPAGO="${DIRBASESPLITTER}/prov/${CARRIER}/opr/output/${PORTAL}/pre-pago"
		UPTOPREPAGO="${DIRBASEPORTAL}/pre-pago"

		ARQLOGIN=$HOME/etc/login_${PORTAL}_${CARRIER}.cfg

#		if [ "${PORTAL}" = "acotel" ]
#		then
#		UPTOPOSPAGO="${CARRIER}/pos-pago"
#		UPTOPREPAGO="${CARRIER}/pre-pago"
#		fi


		echo $ARQLOGIN
		if [ "${IDFILE}" = "opr" ] 
		then	
			/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${UPFROMPOSPAGO} ${UPTOPOSPAGO} ${PORTAL} ${ARQLOGIN}
			/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${UPFROMPREPAGO} ${UPTOPREPAGO} ${PORTAL} ${ARQLOGIN}
		fi
		if [ "${IDFILE}" = "ret" ] 
		then	
			/usr/bin/sh $HOME/bin/download_routine.sh ${ARQLOG} ${DOWNFROMFILERET} ${DOWNTOFILERET} ${PORTAL} ${IP} ${ARQLOGIN}
		fi
		if [ "${IDFILE}" = "sts" ] 
		then	
			/usr/bin/sh $HOME/bin/download_routine.sh ${ARQLOG} ${DOWNFROMFILESTS} ${DOWNTOFILESTS} ${PORTAL} ${IP} ${ARQLOGIN}
		fi
		if [ "${IDFILE}" = "rsp" ] 
		then	
			/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${UPFROMRSPPRE} ${UPTORSPPRE} ${PORTAL} ${ARQLOGIN}
			/usr/bin/sh $HOME/bin/upload_routine.sh ${ARQLOG} ${UPFROMRSPPOS} ${UPTORSPPOS} ${PORTAL} ${ARQLOGIN}
		fi
		if [ "${IDFILE}" = "rqt" ] 
		then	
			/usr/bin/sh $HOME/bin/download_routine.sh ${ARQLOG} ${DOWNFROMRQTPRE} ${DOWNTORQTPRE}  ${PORTAL} ${IP} ${ARQLOGIN}
			/usr/bin/sh $HOME/bin/download_routine.sh ${ARQLOG} ${DOWNFROMRQTPOS} ${DOWNTORQTPOS} ${PORTAL} ${IP} ${ARQLOGIN}
		fi
	done
done

DATAFULL=`date '+%d/%m/%y %T %a'`
echo "Finished at ${DATAFULL}" | tee -a ${ARQLOG}
#/bin/mail -s "Transfer Files" ${MANAGERS} < ${ARQLOG}
/usr/bin/mailx -s "${PORTAL}/${CARRIERS} - ${IDFILE} transfer Files" ${MANAGERS} < ${ARQLOG} 
